﻿using System;

namespace SSISTDD
{
    public static class Sequence
    {
        private static readonly object m_lock = new object();

        private static int _int = 0;
        public static int NextInt
        {
            get
            {
                lock (m_lock)
                {

                    if (_int == Int32.MaxValue)
                        _int = 0;

                    _int++;
                    LogSequenceMessage(_int.ToString());
                    return _int;
                }
            }
        }

        public static string PrefixWithChar(this int value, char character, int totalLength)
        {
            int prefixLength = totalLength - value.ToString().Length;
            if (prefixLength < 0)
                throw new ArgumentOutOfRangeException("Length of supplied value is greater that Total length of prerix");
            return new string(character, prefixLength) + value.ToString();
        }

        private static int _guidInt = 0;
        private static string _guidValue = "00000000-0000-0000-0000-000000000000";
        public static Guid NextGuid
        {
            get
            {
                lock (m_lock)
                {
                    int lastGuidSegment = int.Parse(_guidValue.Substring(24));
                    lastGuidSegment++;
                    string zeros = new string('0', 12 - lastGuidSegment.ToString().Length);
                    _guidValue = "00000000-0000-0000-0000-" + zeros + lastGuidSegment.ToString();
                    _guidInt++;
                    LogSequenceMessage(_guidValue);
                    return new Guid(_guidValue);
                }
            }
        }

        private static int _lsn = 0;
        public static byte[] NextLsn
        {
            get
            {
                lock (m_lock)
                {

                    if (_lsn == Int32.MaxValue)
                        _lsn = 0;

                    _lsn++;
                    return _lsn.ToLsn();
                }
            }
        }

        public static byte[] ToLsn(this int value)
        {
            /// Eugene Niemand
            /// 4/1/2015
            /// This jigary pokery is done because EF always returns byte[10] for LSN's in DB. 
            /// When you GetBytes() it only sizes the array to the minimum size it needs. 
            /// ex. int 275 == byte[4] so NUnit.Assert.AreEqual(byte[4] GeneratedLsn, byte[10] DbLsn) fails
            /// because the array sizes are different.
            byte[] tmpLsn = BitConverter.GetBytes(value);
            byte[] newLsn = new byte[10];
            for (int i = 0; i < 10; i++)
            {
                if (tmpLsn.Length > i)
                {
                    newLsn[i] = tmpLsn[i];
                }
                else
                {
                    newLsn[i] = 0;
                }
            }
            var StrLsn = BitConverter.ToString(newLsn).Replace("-", string.Empty);
            StrLsn = string.Format("0x{0}{1}", StrLsn, new String('0', 20 - StrLsn.Length));
            LogSequenceMessage(StrLsn);
            return newLsn;
        }

        private static void LogSequenceMessage(string StrMessage)
        {
            System.Diagnostics.StackTrace x = new System.Diagnostics.StackTrace();
            Console.WriteLine(string.Format("NextLsn  : {0} : {1}", x.GetFrame(1).GetMethod().Name, StrMessage));
        }
    }
}
