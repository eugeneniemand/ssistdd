﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public static class RandomGenerator
    {
        private static readonly Random Random = new Random();

        public static int Int()
        {
            return Random.Next();
        }

        public static int Int(int min, int max)
        {
            return Random.Next(min, max);
        }

        public static decimal Decimal()
        {
            return decimal.Parse(Random.Next().ToString() + "." + Int(0, 99).ToString());
        }

        public static decimal Decimal(int min, int max)
        {
            return decimal.Parse(Random.Next(min, max).ToString() + "." + Int(0, 99).ToString());
        }

        public static DateTime DateTime()
        {
            int Year = Random.Next(1999, 2015);
            int Month = Random.Next(1, 12);
            int Day = Random.Next(1, 28);
            int Hour = Random.Next(0, 23);
            int Minute = Random.Next(0, 59);
            int Second = Random.Next(0, 59);
            return new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        public static DateTime DateTime(DateTime MinDateTime, DateTime MaxDateTime)
        {
            DateTime RandomDateTime = new DateTime();
            string InvalidDateMessage = "";
            bool InvalidDate = false;
            int InvalidDateRetryCounter = 0;

            int Year = 0;
            int Month = 0;
            int Day = 0;
            int Hour = 0;
            int Minute = 0;
            int Second = 0;

            // this retry logic is used in case an invalid random date is generated.
            do
            {
                InvalidDate = true;
                Year = Random.Next(MinDateTime.Year, MaxDateTime.Year);
                Month = Random.Next(MinDateTime.Month, MaxDateTime.Month);
                Day = Random.Next(MinDateTime.Day, MaxDateTime.Day);
                Hour = Random.Next(MinDateTime.Hour, MaxDateTime.Hour);
                Minute = Random.Next(MinDateTime.Minute, MaxDateTime.Minute);
                Second = Random.Next(MinDateTime.Second, MaxDateTime.Second);
                try
                {
                    RandomDateTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
                    InvalidDate = false;
                }
                catch (Exception ex)
                {
                    InvalidDateRetryCounter++;
                    InvalidDateMessage += string.Format("Try {0}: new DateTime({1}, {2}, {3}, {4}, {5}, {6})\n", InvalidDateRetryCounter, Year, Month, Day, Hour, Minute, Second);
                    if (InvalidDateRetryCounter > 5)
                        throw new ArgumentOutOfRangeException("No valid random date was generated after 5 retries!!!\n" + InvalidDateMessage, ex);
                }
            } while (InvalidDate);
            return RandomDateTime;
        }
    }
}
