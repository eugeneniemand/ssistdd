﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public class Filter
    {
        public string PropertyName { get; set; }
        public object Value { get; set; }
    }

    public class ExpressionBuilder
    {
        public static Expression<Func<T, bool>> BuildFilterFor<T>(object Entity)
        {
            List<Filter> filters = new List<Filter>();
            foreach (var prop in Entity.GetType().GetProperties())
            {
                if (!prop.Name.StartsWith("Hds") && prop.GetValue(Entity) != null)
                {
                    if (prop.PropertyType != typeof(int) || (prop.PropertyType == typeof(int) && (int)prop.GetValue(Entity) != 0))
                    {
                        filters.Add(new Filter { PropertyName = prop.Name, Value = prop.GetValue(Entity) });
                    }
                }
            }

            ParameterExpression param = Expression.Parameter(typeof(T), "t");
            Expression exp = null;

            if (filters.Count == 1)
                exp = GetExpression(param, filters[0]);
            else if (filters.Count == 2)
                exp = GetExpression(param, filters[0], filters[1]);
            else
            {
                while (filters.Count > 0)
                {
                    var f1 = filters[0];
                    var f2 = filters[1];

                    if (exp == null)
                        exp = GetExpression(param, filters[0], filters[1]);
                    else
                        exp = Expression.AndAlso(exp, GetExpression(param, filters[0], filters[1]));

                    filters.Remove(f1);
                    filters.Remove(f2);

                    if (filters.Count == 1)
                    {
                        exp = Expression.AndAlso(exp, GetExpression(param, filters[0]));
                        filters.RemoveAt(0);
                    }
                }
            }

            return Expression.Lambda<Func<T, bool>>(exp, param);
        }

        private static Expression GetExpression(ParameterExpression param, Filter filter)
        {
            MemberExpression member = Expression.Property(param, filter.PropertyName);
            ConstantExpression constant = Expression.Constant(filter.Value, filter.Value.GetType());
            if (Nullable.GetUnderlyingType(member.Type) != null)
                return Expression.Equal(Expression.Convert(member, filter.Value.GetType()), constant);
            else
                return Expression.Equal(member, constant);
        }

        private static BinaryExpression GetExpression(ParameterExpression param, Filter filter1, Filter filter2)
        {
            Expression bin1 = GetExpression(param, filter1);
            Expression bin2 = GetExpression(param, filter2);

            return Expression.AndAlso(bin1, bin2);
        }
    }
}
