﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public interface IEntityContainer
    {
        //string ClearDownCommand { get; }
        string ClearDownCommand(bool keepDefault, bool disableConstraints);
        Dictionary<int, object> Entities { get; set; }
        Dictionary<string, object>[] DefaultValues { get; set; }
    }

    public interface IEntity<T> : IEntityContainer
    {
        T NewEntity(bool withHdsDefaults, Dictionary<string, object>[] defaultValues);
    }
}
