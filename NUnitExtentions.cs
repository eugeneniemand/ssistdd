﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SSISTDD
{
    public class AssertExtentions
    {
        //Iterate through all expected object properties and try to match coresponding properties in actual object
        //Surrogate key on GroupDw is ignored as there are no any in mirror tables
        //Ignore default properties as they are part of the class
        public static void AreAllPropertiesEqual(object expected, object actual)
        {
            Compare(expected, actual, "");
        }

        public static void AreAllPropertiesEqual(object expected, object actual, params string[] excludeProperties)
        {
            Compare(expected, actual, excludeProperties);
        }

        public static string AssertInfoMessage(string RowIdentifier, string AdditionalInfo = null)
        {
            return string.Format("{0} {1}", RowIdentifier, AdditionalInfo);
        }

        private static void Compare(object expected, object actual, params string[] excludeProperties)
        {
            foreach (PropertyInfo propertyInfo in expected.GetType().GetProperties())
            {
                if (!propertyInfo.Name.Equals("DefaultValues") && !excludeProperties.Contains(propertyInfo.Name))
                {
                    try
                    {
                        var actualProperty = actual.GetType().GetProperty(propertyInfo.Name);
                        var expectedProperty = expected.GetType().GetProperty(propertyInfo.Name);

                        if (actualProperty != null)
                        {
                            //Get values from properties
                            object expectedValue = expectedProperty.GetValue(expected, null);
                            object actualValue = actualProperty.GetValue(actual, null);

                            //Compare values
                            Assert.AreEqual(expectedValue, actualValue, string.Format("{0} are not equal", propertyInfo.Name));
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(string.Format("\n!!! An unexpected exception occured while comparing values for property: {0}: {1}\n", propertyInfo.Name, e.Message));
                        throw e;
                    }
                }
            } 
        }
    }
}
