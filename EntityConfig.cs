﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public class RuntimeConfig : ConfigurationSection
    {        
        [ConfigurationProperty("sourceConfigs")]
        public SourceConfigCollection SourceConfigs
        {
            get
            {                
                return (SourceConfigCollection)base["sourceConfigs"];
            }
        }

        [ConfigurationProperty("ssisConfigs")]
        public SsisConfigCollection SsisConfigs
        {
            get
            {
                return (SsisConfigCollection)base["ssisConfigs"];
            }
        }        
    }

    #region SourceConfig
    [ConfigurationCollection(typeof(DbConfig), AddItemName = "sourceConfig", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class SourceConfigCollection : ConfigurationElementCollection
    {
        public new ConfigurationElementCollectionType CollectionType
        { get { return ConfigurationElementCollectionType.BasicMap; } }

        protected override string ElementName
        {
            get
            {
                return "sourceConfig";
            }
        }

        public SourceConfig this[SourceConfigsEnum name]
        {
            get
            {
                return (SourceConfig)base.BaseGet(name.ToString());
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            { return new SourceConfig(); }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as SourceConfig).Name;
        }
    }

    public class SourceConfig : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)base["name"];
            }
        }

        [ConfigurationProperty("ssisEnvironment", IsRequired = true)]
        public SsisEnvironment SsisEnvironment
        {
            get
            {
                return (SsisEnvironment)base["ssisEnvironment"];
            }
        }

        [ConfigurationProperty("dbConfigs")]
        public DbConfigCollection DbConfigs
        {
            get
            {
                return (DbConfigCollection)base["dbConfigs"];
            }
        }
    }

    public class SsisEnvironment : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)base["name"];
            }
        }
    }
    #endregion

    #region DbConfig
    [ConfigurationCollection(typeof(DbConfig), AddItemName = "dbConfig", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class DbConfigCollection : ConfigurationElementCollection
    {
        public new ConfigurationElementCollectionType CollectionType
        { get { return ConfigurationElementCollectionType.BasicMap; } }

        protected override string ElementName
        {
            get
            {
                return "dbConfig";
            }
        }

        public DbConfig this[DbConfigsEnum name]
        {
            get
            {
                return (DbConfig)base.BaseGet(name.ToString());
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            { return new DbConfig(); }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as DbConfig).Name;
        }
    }

    public class DbConfig : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)base["name"];
            }
        }
       
        [ConfigurationProperty("databaseName", IsRequired = true)]
        public string DatabaseName
        {
            get
            {
                return (string)base["databaseName"];
            }
        }

        [ConfigurationProperty("modelFullName", IsRequired = true)]
        public string ModelFullName
        {
            get
            {
                return (string)base["modelFullName"];
            }
        }
    }    
    #endregion

#region ssisConfigs
    [ConfigurationCollection(typeof(DbConfig), AddItemName = "ssisConfig", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class SsisConfigCollection : ConfigurationElementCollection
    {
        public new ConfigurationElementCollectionType CollectionType
        { get { return ConfigurationElementCollectionType.BasicMap; } }

        protected override string ElementName
        {
            get
            {
                return "ssisConfig";
            }
        }

        public SsisConfig this[SsisConfigsEnum name]
        {
            get
            {
                return (SsisConfig)base.BaseGet(name.ToString());
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            { return new SsisConfig(); }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as SsisConfig).Name;
        }
    }

    public class SsisConfig : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)base["name"];
            }
        }
        
        [ConfigurationProperty("projectName", IsRequired = true)]
        public string ProjectName
        {
            get
            {
                return (string)base["projectName"];
            }
        }

        [ConfigurationProperty("folderName", IsRequired = true)]
        public string FolderName
        {
            get
            {
                return (string)base["folderName"];
            }
        }

    }
#endregion
}
