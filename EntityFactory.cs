using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;

namespace SSISTDD
{
    public class EntityFactory<T> : IEntity<T> where T : class
    {
        public Dictionary<int, object> Entities { get; set; }
        public Dictionary<string, object>[] DefaultValues { get; set; }
        private EntityHelper<T> eh = new EntityHelper<T>();
        private IDbController entityController;

        internal EntityFactory(IDbController EntityController)
        {
            entityController = EntityController;
            Entities = new Dictionary<int, object>();
        }

        /// <summary>
        /// This returns an instance of Entity of type T and adds the Entity to the Entities collection in the Entity Factory
        /// </summary>
        /// <param name="setDefaults">If this is true it will set the defaults on the entity provided by the defaults parameter</param>
        /// <param name="defaults">This is a Dictionary where the Key and valaue should match the Entity Property and datatype</param>
        /// <returns>A new Entity(record) of type T</returns>
        public T NewEntity(bool setDefaults, params Dictionary<string, object>[] defaults)
        {
            DefaultValues = defaults;
            if (setDefaults)
                return eh.SetDefaults(eh.AddNewEntity(Entities), DefaultValues);
            else
                return eh.AddNewEntity(Entities);
        }

        public void SetDefaults()
        {
            eh.SetDefaults(eh.AddNewEntity(Entities), DefaultValues);
        }

        public string ClearDownCommand(bool keepDummyRecords, bool disableConstraints)
        {
            ResourceManager defaultResourceManager = new ResourceManager("SSISTDD.Properties.Resources", Assembly.GetExecutingAssembly());
            if (keepDummyRecords)
            {
                return string.Format(defaultResourceManager.GetString("SqlDeleteKeepDefaultStatement"), eh.GetTableName(entityController.db));
            }
            else if (!keepDummyRecords && disableConstraints)
            {
                return string.Format(defaultResourceManager.GetString("SqlTruncateStatement"), eh.GetTableName(entityController.db));
            }
            else
            {
                return string.Format(defaultResourceManager.GetString("SqlDeleteStatement"), eh.GetTableName(entityController.db));
            }
        }

        public bool Exists(object Entity)
        {
            var filterExpression = ExpressionBuilder.BuildFilterFor<T>(Entity);
            var _resultsCount = entityController.db.Set<T>().Where(filterExpression).Count();
            return _resultsCount >= 1;
        }

        /// <summary>
        /// Returns the First Entity returned by the predicate in an un ordered result set
        /// </summary>
        /// <param name="predicate">WHERE Clause</param>
        /// <param name="TrackChanges">If you need to update a record set to TRUE, beware if track changes is TRUE and an update happens in DB,
        /// you will have to fetch entiy again with value set to FALSE or EF will read values from cache and not from DB</param>
        /// <returns></returns>
        public T Where(Expression<Func<T, bool>> predicate, bool TrackChanges = false)
        {
            if (TrackChanges)
                return entityController.db.Set<T>().Where<T>(predicate).FirstOrDefault();
            else
                return entityController.db.Set<T>().AsNoTracking().Where<T>(predicate).FirstOrDefault();
        }

        public T Where(string ExecProcSQL, Func<T, bool> predicate)
        {
            return entityController.db.Database.SqlQuery<T>(ExecProcSQL).Where(predicate).FirstOrDefault();
        }

        /// <summary>
        /// Returns List&lt;T&gt; of all Entities returned by the predicate in an un ordered result set
        /// </summary>
        /// <param name="predicate">WHERE Clause</param>
        /// <param name="TrackChanges">If you need to update a record set to TRUE, beware if track changes is TRUE and an update happens in DB,
        /// you will have to fetch entiy again with value set to FALSE or EF will read values from cache and not from DB</param>
        /// <returns></returns>
        public List<T> WhereAll(Expression<Func<T, bool>> predicate, bool TrackChanges = false)
        {
            if (TrackChanges)
                return entityController.db.Set<T>().Where<T>(predicate).ToList<T>();
            else
                return entityController.db.Set<T>().AsNoTracking().Where<T>(predicate).ToList<T>();
        }

        public List<T> All(bool TrackChanges = false)
        {
            if (TrackChanges)
                return entityController.db.Set<T>().ToList<T>();
            else
                return entityController.db.Set<T>().AsNoTracking().ToList<T>();
        }

        public List<T> ResultsFromProc(string StoredProcedureSchema, string StoredProcedureName)
        {
            return entityController.db.Database.SqlQuery<T>(string.Format("EXEC {0}.{1}", StoredProcedureSchema, StoredProcedureName)).ToList<T>();
        }
    }
}
