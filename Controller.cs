﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Resources;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace SSISTDD
{
    public class DbController : IDbController
    {
        public List<IEntityContainer> entityContainers = new List<IEntityContainer>();
        public DbContext db { get; set; }

        /// <summary>
        /// This method will clear down all tables that are currently associated with the DbController in scope. 
        /// Set keepDummyRecords = false and disableConstraints = true to issue a truncate statement instead of a delete statement.
        /// </summary>
        /// <param name="keepDummyRecords">True will issue a delete statement where the PK &lt; 0 for i.e. keepr for example Invalid and Unknown records</param>
        /// <param name="disableConstraints">This will disable all constraints before clearing down tables and re-enable them afterwards</param>        
        public void ClearDown(bool keepDummyRecords, bool disableConstraints)
        {
            ResourceManager defaultResourceManager = new ResourceManager("SSISTDD.Properties.Resources", Assembly.GetExecutingAssembly());
            if (disableConstraints)
            {
                db.Database.ExecuteSqlCommand(defaultResourceManager.GetString("DisableConstraints"));
            }

            foreach (IEntityContainer entity in entityContainers)
            {
                Console.WriteLine(string.Format("{0}.ClearDown() using Command: {1}", entity.ToString(), entity.ClearDownCommand(keepDummyRecords, disableConstraints)));
                db.Database.ExecuteSqlCommand(entity.ClearDownCommand(keepDummyRecords, disableConstraints));
            }

            if (disableConstraints)
            {
                db.Database.ExecuteSqlCommand(defaultResourceManager.GetString("EnableConstraints"));
            }
        }

        private void AddEntityFactory(IEntityContainer entityFactory)
        {
            entityContainers.Add(entityFactory);
        }

        /// <summary>
        /// The Entity Factory is used to provide new Entities(records), a Entity enumerator and the Entity's cleardown command. It also associates the Entity to the DbController.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>A EntityFactory instance for the specified type T</returns>
        public EntityFactory<T> GetEntityFactory<T>() where T : class
        {
            EntityFactory<T> tmpFactory = new EntityFactory<T>(this);
            AddEntityFactory(tmpFactory);
            return tmpFactory;
        }
      
        /// <summary>
        /// This will insert all entities that were created using the NewEntity method
        /// </summary>
        public void Insert()
        {
            try
            {
                foreach (IEntityContainer container in entityContainers)
                {
                    foreach (var entity in container.Entities.Values)
                    {
                        var x = db.Set(entity.GetType());
                        x.Add(entity);
                    }
                }
                db.SaveChanges();
            }            
            catch (DbEntityValidationException e)
            {
                string errorMessage = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    errorMessage += string.Format("\n!!! Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errorMessage += string.Format("\n\t - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                Console.WriteLine(errorMessage);
                throw new DbEntityValidationException(errorMessage);
            }
            catch (DbUpdateException e)
            {
                string errorMessage = "";
                if (e.InnerException == null)
                {
                    errorMessage = string.Format("\n!!! Does table have a primary key defined? If not add PK and refresh EF and check model's key mathces table's PK\n{0}", e.Message);
                }
                else
                {
                    Exception ex = e;
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                        errorMessage = string.Format("\n!!! Does table have a primary key defined? If not add PK and refresh EF and check model's key mathces table's PK\n{0}", ex.Message);
                    }                    
                }
                Console.WriteLine(errorMessage);
                throw new DbUpdateException(errorMessage);
            }
        }

        /// <summary>
        /// This will update all entities that were created using the NewEntity method
        /// </summary>
        public void Update()
        {
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                string errorMessage = string.Format("\n!!! Does table have a primary key defined? If not add PK and refresh EF and check model's key mathces table's PK\n{0}", e.Message);
                Console.WriteLine(errorMessage);
                throw new DbUpdateException(errorMessage);
            }           
        }
    }
}
