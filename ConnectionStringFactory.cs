﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public static class ConnectionStringFactory
    {
        public static string ChangeDatabaseInConnectionString(string connectionString, string databaseName)
        {
            SqlConnectionStringBuilder cn = new SqlConnectionStringBuilder(connectionString);
            cn["Initial Catalog"] = databaseName;
            return cn.ConnectionString;
        }
        
    }
}
