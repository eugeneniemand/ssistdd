﻿using System;
using System.Configuration;

namespace SSISTDD
{
    public class DbConfigsEnum
    {
        public string Value { get; private set; }
        public override string ToString()
        {
            return this.Value;
        }

        protected DbConfigsEnum(string value)
        {
            this.Value = value;
        }        
    }

    public class SourceConfigsEnum
    {
        public string Value { get; private set; }
        public override string ToString()
        {
            return this.Value;
        }

        protected SourceConfigsEnum(string value)
        {
            this.Value = value;
        }
    }

    public class SsisConfigsEnum
    {
        public string Value { get; private set; }
        public override string ToString()
        {
            return this.Value;
        }

        protected SsisConfigsEnum(string value)
        {
            this.Value = value;
        }
    }

    public class ConfigController
    {
        private RuntimeConfig _runtimeConfig;
        private SourceConfigsEnum _sourceConfig;
        private DbConfigsEnum _dbConfig;
        private SsisConfigsEnum _ssisConfig;

        public ConfigController(SourceConfigsEnum sourceConfig, DbConfigsEnum dbConfig)
        {
            _runtimeConfig = (RuntimeConfig)ConfigurationManager.GetSection("RuntimeConfig");
            _sourceConfig = sourceConfig;
            _dbConfig = dbConfig;
            if (_runtimeConfig.SourceConfigs[_sourceConfig] == null)
                throw new NullReferenceException(_sourceConfig + " sourceConfig was not found in the App.config file. Please ensure that sourceConfig section exists");
            if (_runtimeConfig.SourceConfigs[_sourceConfig].DbConfigs[_dbConfig] == null)
                throw new NullReferenceException(_dbConfig + " dbConfig was not found in the App.config file. Please ensure that dbConfig exists");
        }

        public ConfigController(SourceConfigsEnum sourceConfig, SsisConfigsEnum ssisConfig)
        {
            _runtimeConfig = (RuntimeConfig)ConfigurationManager.GetSection("RuntimeConfig");
            _sourceConfig = sourceConfig;
            _ssisConfig = ssisConfig;
            if (_runtimeConfig.SourceConfigs[_sourceConfig] == null)
                throw new NullReferenceException(_sourceConfig + " sourceConfig was not found in the App.config file. Please ensure that sourceConfig section exists");
            if (_runtimeConfig.SourceConfigs[_sourceConfig].SsisEnvironment == null)
                throw new NullReferenceException(_ssisConfig + " SsisEnvironment was not found in the App.config file. Please ensure that SsisEnvironment attribute exists");
        }

        public string SsisEnvironment
        {
            get
            {
                return _runtimeConfig.SourceConfigs[_sourceConfig].SsisEnvironment.Name;
            }
        }

        public string ModelFullName
        {
            get
            {
                return _runtimeConfig.SourceConfigs[_sourceConfig].DbConfigs[_dbConfig].ModelFullName;
            }
        }

        public string DatabaseName
        {
            get
            {
                return _runtimeConfig.SourceConfigs[_sourceConfig].DbConfigs[_dbConfig].DatabaseName;
            }
        }

        public string SsisProjectName
        {
            get
            {
                return _runtimeConfig.SsisConfigs[_ssisConfig].ProjectName;
            }
        }

        public string SsisFolderName
        {
            get
            {
                return _runtimeConfig.SsisConfigs[_ssisConfig].FolderName;
            }
        }
    }
}
