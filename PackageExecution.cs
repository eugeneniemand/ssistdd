﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.IntegrationServices;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters;

namespace SSISTDD
{
    public class PackageExecution
    {
        private static bool errorOccured;
        private static string errorMessages;
        private static Collection<PackageInfo.ExecutionValueParameterSet> CreateDefaultParameters()
        {
            return new Collection<PackageInfo.ExecutionValueParameterSet>
                {
                    new PackageInfo.ExecutionValueParameterSet
                        {
                            ObjectType = 50,
                            ParameterName = "SYNCHRONIZED",
                            ParameterValue = 1
                        }
                };
        }

        public static void Execute(string folder, string project, string package, IDictionary<string, object> projectParameters = null, IDictionary<string, object> packageParameters = null, string environment = null)
        {
            //ExecuteManagedCode(folder, project, package, projectParameters, packageParameters, environment);
            ExecuteSQL(folder, project, package, projectParameters, packageParameters, environment);
            //ExecuteQueue(folder, project, package, projectParameters, packageParameters, environment);
        }

        public static void ExecuteQueue(string folder, string project, string package, IDictionary<string, object> projectParameters = null, IDictionary<string, object> packageParameters = null, string environment = null)
        {
            int lastExecutionStatus = ExecutionStatusForPackage(folder, project, package);
            int timeout = 10;
            int timeoutcounter = 0;
            int pollInterval = 250;
            QueuePackage(folder, project, package, projectParameters, packageParameters);
            if (!TestControllerIsRunning())
            {
                ExecuteSQL(folder, project, "TestController.dtsx", projectParameters, null);
            }
            while (lastExecutionStatus == ExecutionStatusForPackage(folder, project, package))
            {
                System.Threading.Thread.Sleep(pollInterval);
                timeoutcounter++;
                if (timeoutcounter > (timeout * 1000) / pollInterval)
                {
                    throw new Exception(String.Format("Waiting for package '{0}' timed-out", package));
                }
            }
        }

        private static string ToJsonString(object collection)
        {
            return JsonConvert.SerializeObject(collection, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
            });
        }

        private static void QueuePackage(string folder, string project, string package, IDictionary<string, object> projectParameters = null, IDictionary<string, object> packageParameters = null, string environment = null)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SSISTDD"].ConnectionString;
            SqlConnection cn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(string.Format(@"insert SSISTDD_PackageQueue (PackageName, PackageParams) values ('{0}', '{1}')", package, ToJsonString(packageParameters)), cn);
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cn.Close();
            }
        }

        private static int ExecutionStatusForPackage(string folder, string project, string package)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SSISTDD"].ConnectionString;
            SqlConnection cn = new SqlConnection(connectionString);
            //SqlCommand cmd = new SqlCommand(string.Format(@"select max([executions].execution_id) from  [catalog].[packages] inner join catalog.executables on executable_guid = package_guid inner join [catalog].[executions] on executables.execution_id = executions.execution_id where name = '{0}' and executions.folder_name = '{1}' and executions.project_name = '{2}'", package, folder, project), cn);
            SqlCommand cmd = new SqlCommand(string.Format(@"select top 1 Executed from SSISTDD_PackageQueue where PackageName = '{0}' order by id desc", package), cn);
            int lastExecutionId = -1;
            try
            {
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr[0] != DBNull.Value)
                        lastExecutionId = Convert.ToInt32(dr[0]);
                }

            }
            finally
            {
                cn.Close();
            }
            return lastExecutionId;
        }

        private static bool TestControllerIsRunning()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SSIS_Catalog"].ConnectionString;
            SqlConnection cn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("select 1 from [catalog].[executions] where package_name = 'TestController.dtsx' and status in (2,5)", cn);
            bool isRunning = false;
            try
            {
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                isRunning = dr.HasRows;
            }
            finally
            {
                cn.Close();
            }
            return isRunning;
        }

        private static void ExecuteSQL(string folder, string project, string package, IDictionary<string, object> projectParameters = null, IDictionary<string, object> packageParameters = null, string environment = null)
        {
            string additionalParameters = "";
            int varCounter = 0;
            if (projectParameters != null)
                foreach (var projectParameter in projectParameters)
                {
                    varCounter++;
                    string sqlVarTpe = "";
                    string sqlVar = "";
                    string sqlName = string.Format("@var{0}", varCounter);
                    switch (projectParameter.Value.GetType().Name.ToString())
                    {
                        case "Boolean":
                            sqlVarTpe = string.Format("declare {0} bit", sqlName);
                            break;
                        case "Int16":
                        case "Int32":
                        case "Int64":
                            sqlVarTpe = string.Format("declare {0} int", sqlName);
                            break;
                        default:
                            sqlVarTpe = string.Format("declare {0} nvarchar(255)", sqlName);
                            break;
                    }
                    switch (projectParameter.Value.GetType().Name.ToString())
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "Boolean":
                            sqlVar = string.Format("{1}", sqlVarTpe, (bool)projectParameter.Value ? 1 : 0);
                            break;
                        default:
                            sqlVar = string.Format("'{1}'", sqlVarTpe, projectParameter.Value.ToString());
                            break;
                    }

                    additionalParameters += string.Format(@"
                                            {1} = {2}
                                            exec [catalog].[set_execution_parameter_value]
                                                    @execution_id,
                                                    @object_type = 20,
                                                    @parameter_name = N'{0}',
                                                    @parameter_value = {3}
                                            ", projectParameter.Key, sqlVarTpe, sqlVar, sqlName);
                    Console.WriteLine(string.Format(@"Project Parameter: {0} = {1}", projectParameter.Key, projectParameter.Value.ToString()));
                }

            if (packageParameters != null)
                foreach (var packageParameter in packageParameters)
                {
                    varCounter++;
                    string sqlVarTpe = "";
                    string sqlVar = "";
                    string sqlName = string.Format("@var{0}", varCounter);
                    switch (packageParameter.Value.GetType().Name.ToString())
                    {
                        case "Boolean":
                            sqlVarTpe = string.Format("declare {0} bit", sqlName);
                            break;
                        case "Int16":
                        case "Int32":
                        case "Int64":
                            sqlVarTpe = string.Format("declare {0} int", sqlName);
                            break;
                        default:
                            if (packageParameter.Value.GetType().BaseType.Name != "Enum")
                                sqlVarTpe = string.Format("declare {0} nvarchar(255)", sqlName);
                            else
                            {
                                sqlVarTpe = string.Format("declare {0} int", sqlName);
                            }
                            break;
                    }
                    switch (packageParameter.Value.GetType().Name.ToString())
                    {
                        case "Boolean":
                            sqlVar = string.Format("{1}", sqlVarTpe, (bool)packageParameter.Value ? 1 : 0);
                            break;
                        case "Int16":
                        case "Int32":
                        case "Int64":
                            sqlVar = string.Format("{1}", sqlVarTpe, packageParameter.Value);
                            break;
                        default:
                            if (packageParameter.Value.GetType().BaseType.Name != "Enum")
                                sqlVar = string.Format("'{1}'", sqlVarTpe, packageParameter.Value.ToString());
                            else
                            {
                                sqlVar = string.Format("'{1}'", sqlVarTpe, ((int)packageParameter.Value).ToString());
                            }

                            break;
                    }

                    additionalParameters += string.Format(@"
                                            {1} = {2}
                                            exec [catalog].[set_execution_parameter_value]
                                                    @execution_id,
                                                    @object_type = 30,
                                                    @parameter_name = N'{0}',
                                                    @parameter_value = {3}
                                            ", packageParameter.Key, sqlVarTpe, sqlVar, sqlName);
                    Console.WriteLine(string.Format(@"Package Parameter: {0} = {1}", packageParameter.Key, packageParameter.Value.ToString()));
                }
            Console.WriteLine(additionalParameters);

            string connectionString = ConfigurationManager.ConnectionStrings["SSIS_Catalog"].ConnectionString;
            SqlConnection cn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(string.Format(@"
            declare
                @ProjectName nvarchar(128) = '{0}',
                @FolderName nvarchar(100) = '{1}',
                @PackageName nvarchar(260) = '{2}'	
            begin
                declare
                    @Reference_id bigint,
                    @execution_id bigint

                set @Reference_id = (
                                        select
                                            e.reference_id
                                        from
                                            catalog.environment_references e
                                            inner join catalog.projects p
                                                on e.project_id = p.project_id
                                        where
                                            p.name = @ProjectName
                                            and environment_folder_name = @FolderName
                                            and environment_name = '{3}'
                                    )
                exec [catalog].[create_execution]
                    @package_name = @PackageName,
                    @execution_id = @execution_id output,
                    @folder_name = @FolderName,
                    @project_name = @ProjectName,
                    @use32bitruntime = False,
                    @reference_id = @Reference_id
    
                {4}

                exec [catalog].[start_execution]
                    @execution_id

                while (
                        select
                            e.end_time
                        from
                            catalog.executions as e
                        where
                            e.execution_id = @execution_id
                        ) is null
                    begin	
                        waitfor delay '00:00:00.010'
                    end
                
                select
                    Message,Message_Type
                from
                    catalog.event_messages as em
                where
                    em.operation_id = @execution_id
                    and Message_Type = 120
            end
            ", project, folder, package, environment, additionalParameters), cn);

            errorOccured = false;
            errorMessages = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ERRORS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
            Console.WriteLine(string.Format(@"RunPackage {0}\{1}\{2}", folder, project, package));
            try
            {
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    errorMessages += "\t\t" + dr["Message"].ToString().Replace("\n", "\n\t\t");
                    errorOccured = true;
                }
            }
            finally
            {
                cn.Close();
            }
            if (errorOccured)
            {
                Console.WriteLine(errorMessages);
                throw new ApplicationException(string.Format("\n\nExceptions Occured in packageName: {0}\\{1}\\{2}.\n{3}", folder, project, package, errorMessages));
            }
        }
        private static void ExecuteManagedCode(string folder, string project, string package, IDictionary<string, object> projectParameters = null, IDictionary<string, object> packageParameters = null, string environment = null)
        {
            errorOccured = false;
            errorMessages = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ERRORS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";

            Console.WriteLine(string.Format(@"RunPackage {0}\{1}\{2}", folder, project, package));
            Collection<PackageInfo.ExecutionValueParameterSet> additionalParameters = CreateDefaultParameters();

            if (projectParameters != null)
                foreach (var projectParameter in projectParameters)
                {
                    additionalParameters.Add(
                        new PackageInfo.ExecutionValueParameterSet
                            {
                                // Add a parameter collection for 'system' parameters (ObjectType = 50), packageName parameters (ObjectType = 30) and project parameters (ObjectType = 20)
                                ObjectType = 20,
                                ParameterName = projectParameter.Key,
                                ParameterValue = projectParameter.Value
                            }
                        );
                    Console.WriteLine(string.Format(@"Project Parameter: {0} = {1}", projectParameter.Key, projectParameter.Value.ToString()));
                }

            if (packageParameters != null)
                foreach (var packageParameter in packageParameters)
                {
                    additionalParameters.Add(
                        new PackageInfo.ExecutionValueParameterSet
                            {
                                // Add a parameter collection for 'system' parameters (ObjectType = 50), packageName parameters (ObjectType = 30) and project parameters (ObjectType = 20)
                                ObjectType = 30,
                                ParameterName = packageParameter.Key,
                                ParameterValue = packageParameter.Value
                            }
                        );
                    Console.WriteLine(string.Format(@"Package Parameter: {0} = {1}", packageParameter.Key, packageParameter.Value.ToString()));
                }

            string connectionString = ConfigurationManager.ConnectionStrings["SSISServer"].ConnectionString;
            package = package.Replace(".dtsx", "");

            // Connection to the database server where the packages are located
            var ssisConnection = new SqlConnection(connectionString);

            // SSIS server object with connection
            var ssisServer = new IntegrationServices(ssisConnection);

            // The reference to the packageName which you want to execute
            PackageInfo ssisPackage = ssisServer.Catalogs["SSISDB"].Folders[folder].Projects[project].Packages[package + ".dtsx"];
            // Get the identifier of the execution to get the log
            if (ssisPackage == null)
                throw new NullReferenceException(string.Format(@"PACKAGE NOT FOUND: {0}\{1}\{2} have you deployed the SSIS projects?", folder, project, package));

            long executionIdentifier = -1;
            if (environment != null)
            {
                ssisServer.Catalogs["SSISDB"].Folders[folder].Projects[project].References.Add(environment);
                EnvironmentReference environmentReference = ssisServer.Catalogs["SSISDB"].Folders[folder].Projects[project].References[environment, folder];
                if (environmentReference == null)
                    throw new NullReferenceException(string.Format(@"ENVIRONMENT NOT FOUND: {0} do you have the right environment value and have you deployed the projects", environment));
                executionIdentifier = ssisPackage.Execute(false, environmentReference, additionalParameters);
            }
            else
                executionIdentifier = ssisPackage.Execute(false, null, additionalParameters);

            foreach (OperationMessage message in ssisServer.Catalogs["SSISDB"].Executions[executionIdentifier].Messages)
            {
                if (message.MessageType == 120)
                {
                    errorMessages += "\t\t" + message.Message.Replace("\n", "\n\t\t");
                    errorOccured = true;
                }
                Console.WriteLine((message.MessageType + ": " + message.Message));
            }

            if (errorOccured)
                throw new ApplicationException(string.Format("\n\nExceptions Occured in packageName: {0}\\{1}\\{2}.\n{3}", folder, project, package, errorMessages));
        }
    }
}