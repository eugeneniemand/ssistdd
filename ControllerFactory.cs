﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Reflection;
using Microsoft.SqlServer.Management.Common;

namespace SSISTDD
{
    public static class DbControllerFactory
    {
        /// <summary>
        /// Returns a DbController object i.e. an interface to interact with the database as specified in the config file
        /// </summary>
        /// <param name="sourceConfig">This determines the name of the source system configuration section to use</param>
        /// <param name="dbConfig">This determines the name of the database config item within the source system configuration section</param>
        /// <returns></returns>
        public static DbController NewDbController(SourceConfigsEnum sourceConfig, DbConfigsEnum dbConfig)
        {
            ConfigController cc = new ConfigController(sourceConfig, dbConfig);
            DbController ctrl = new DbController();
            ctrl.db = CreateInstance(cc.ModelFullName, cc.DatabaseName);
            return ctrl;
        }

        private static DbContext CreateInstance(string modelFullName, string dbName)
        {
            if (Type.GetType(modelFullName) == null)
            {
                var x = Assembly.GetCallingAssembly().GetTypes();
                throw new InvalidArgumentException(string.Format("\n\nERROR IN CONFIG\nIs modelFullName: \"{0}\" correctly configured in the App.config for the Entity Model you are trying to create? Pay attention to Model and Assembly name\n\n", modelFullName));
            }
            var EntityInstance = Activator.CreateInstance(Type.GetType(modelFullName)) as DbContext;
            EntityInstance.Database.Connection.ConnectionString = ConnectionStringFactory.ChangeDatabaseInConnectionString(EntityInstance.Database.Connection.ConnectionString, dbName);
            return EntityInstance;
        }
    }
}
