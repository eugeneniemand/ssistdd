﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SSISTDD
{
    public interface ISsisPackage
    {
        string Folder { get; }
        string Project { get; }
        string Environment { get; }
        string PackageName { get; }
        void RunPackage();
        void RunPackage(Dictionary<string, object> ProjectParams);
        void RunPackage(Dictionary<string, object> ProjectParams, Dictionary<string, object> PackageParams);
    }

    public class SSISPackage : ISsisPackage
    {

        public string Folder { get; private set; }
        public string Project { get; private set; }
        public string PackageName { get; private set; }
        public string Environment { get; private set; }

        public SSISPackage(SourceConfigsEnum sourceConfig, SsisConfigsEnum ssisConfig, string packageName)
        {
            ConfigController cc = new ConfigController(sourceConfig, ssisConfig);
            Folder = cc.SsisFolderName;
            Project = cc.SsisProjectName;
            Environment = cc.SsisEnvironment;
            PackageName = packageName;
        }

        public void RunPackage()
        {
            PackageExecution.Execute(Folder, Project, PackageName, null, null, Environment);
        }

        public void RunPackage(Dictionary<string, object> ProjectParams)
        {
            PackageExecution.Execute(Folder, Project, PackageName, ProjectParams, null, Environment);
        }
       
        ﻿public void RunPackage(Dictionary<string, object> ProjectParams, Dictionary<string, object> PackageParams)
        {
            PackageExecution.Execute(Folder, Project, PackageName, ProjectParams, PackageParams, Environment);
        }         
    }
}
