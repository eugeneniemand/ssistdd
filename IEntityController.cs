﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public interface IDbController
    {
        DbContext db { get; set; }
        void ClearDown(bool keepDefaults, bool disableConstraints);
        void Insert();  
    }
}
