﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISTDD
{
    public static class ExtentionMethods
    {
        /// <summary>
        /// Returns Date as string of supplied date in yyyy-MM-dd format
        /// </summary>
        /// <returns></returns>
        public static int ToDateSK(this DateTime dt)
        {
            return int.Parse(dt.ToString("yyyyMMdd"));
        }

        /// <summary>
        /// Returns Date as string of supplied date in yyyy-MM-dd format
        /// </summary>
        /// <returns></returns>
        public static string ToDateString(this DateTime dt)
        {
            return string.Format("{0:yyyy-MM-dd}", dt);
        }

        /// <summary>
        /// Returns Time as string of supplied date in HH:mm format
        /// </summary>
        /// <returns></returns>
        public static string ToTimeString(this DateTime dt)
        {
            return string.Format("{0:HH:mm}", dt);
        }

        /// <summary>
        /// Returns Date as string of supplied date in yyyy-MM-dd format
        /// </summary>
        /// <returns></returns>
        public static DateTime ToDate(this DateTime dt)
        {
            return DateTime.Parse(dt.ToShortDateString());
        }

        /// <summary>
        /// Returns Date as string of supplied date in yyyy-MM-dd format
        /// </summary>
        /// <returns></returns>
        public static DateTime ToDateTime(this DateTime dt)
        {
            // We only convert the to two milliseconds as datetime only stores to precision of 3 millisecods. 
            // So if we use 3 millisecods, the date stored in DB can be different from date in c#
            // More info here http://stackoverflow.com/questions/715432/why-is-sql-server-losing-a-millisecond
            var ticks = int.Parse(dt.TimeOfDay.ToString().Substring(11));
            dt = dt.AddTicks(-ticks);
            return dt;
        }
    }
}
