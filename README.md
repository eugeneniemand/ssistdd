# PLEASE READ ME
If you have problems, please contact me. This code comes as is, with no guarantees and I will not be responsible for anything not working as intended. Please feel free to submit issues/ideas/feedback etc and if you'd like please contribute via pull requests. I'll make this a public repository as soon as we have someone else using this in production other than us.

# SAMPLE
https://bitbucket.org/eugeneniemand/ssistdd_testcontroller_tdddemo

# Adding a new Entity Model

### 1a. Add A New Entity Model (if it doesn't exist yet)

*   Create folder with name of schema (TitleCase)
*   Add new ADO.NET Entity Data Model
*   Name new item same as the region followed by the schema, i.e. ZaPayment.edmx (camelCase)
*   Genertate from Database
*   Select/Create Connection and save in App.Config if it does not exits yet, using the schema name. i.e "PaymentEntityContainer", your schema will replace Payment (TitleCase)
*   Select the required tables,  **only** within the schema you are creating
*   Change model namespace to match schema (camelCase)
*   Select edmx file in solution again and change Custom Tool Namespace for all files under edmx to: "BI.Test.DataModel.ZaHds.ZaPayment", your schema will replace Payment. (you can select all 4 files at once and then change it)
*   Save the edmx to generate the entity classes

### 1b. Add a new Entity to an existing model

*   Right click on the container and select "Update model from database" option.
*   Select the new table(s) you are adding within the same schema as the current container.
*   Click finish and save the edmx to generate the new entity classes and update existing classes.

### 2\. Update the App.config in the GroupDW.<LAYER>.UnitTests project

If the connection string does not yet exist in the Test project App.config

*   Copy the Connection string from the DataModel project for the entity that you are adding and paste it in the connectionstrings section.

```
#!xml
<add name="PaymentEntityContainer" connectionString="metadata=<a>res://*/HDS.Payment.payment.csdl|res://*/HDS.Payment.payment.ssdl|res://*/HDS.Payment.payment.msl;provider=System.Data.SqlClient;provider</a> connection string=&quot;data source=localhost\sql2012;initial catalog=UK_HDS;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />

```

If no dbConfig exists for the new Entity Model

*   Create a new dbConfig entry
*   name is used to uniquely identify the Entity Model and sill be used to map from the DbConfigEnum to the dbConfig in App.Config
*   databaseName is the database that will be used by Entity Framework at runtime
*   modelFullName is the fully qualified name of the entity model and its container Assembly i.e. the project

**Example**
```
#!xml
<dbConfig name="Payment" databaseName="ZA_HDS" modelFullName="BI.Test.DataModel.HDS.Payment.PaymentEntityContainer, BI.Test.Datamodel" />
```

### 3\. Update the DbConfigEnum in the Framework

When adding a new Entity Model also update the DbConfigEnum in the Framework project (DatabaseEnum.cs) by adding the same name used for the dbConfig in the App.config.

# Using an entity in Test Classes

*   Add the namespace used in the Custom Tool Namespace on the edmx to the class
*   Create a new private field of Type "DbController" i.e. "DbController HDS;"
*   Create a new private field of Type "EntityFactory<T>" where T is your entity class i.e. "EntityFactory<Applications_Hds> paymentApplicationsHds;"
*   In the FixtureSetUp initilaize the DbController and EntityFactory

```
#!csharp
paymentHds = DbControllerFactory.NewDbController(SourceConfigsEnum.V3ZA, DbConfigsEnum.Source);  
paymentApplicationsHds = paymentHds.GetEntityFactory<Applications_Hds>();
```

*   Now you can create new instances of your entity

```
#!csharp
var AApplicationHds = paymentApplicationsHds.NewEntity(true, HdsDefaults.DefaultValues);   
AApplicationHds.ApplicationId = Sequence.NextInt;  
AApplicationHds.ExternalId = Sequence.NextGuid;  
AApplicationHds.ApplicationReference = "Test PENDING_RISK";  
AApplicationHds.SignedOn = RandomDate;
```

*   Before executing pacakges call ClearDown and Insert on relevant DbControllers

```
#!csharp
paymentHds.ClearDown();  
paymentHds.Insert();
```
# Executing a SSIS Package

*   Create a new SSISPackage field
```
#!csharp
SSISPackage extractPackage = new SSISPackage(SourceConfigsEnum.V3ZA, SsisConfigsEnum.V3ExtractConform, "GroupDW.Extract.V3.Payment_Applications.dtsx");
```

*   Execute Pacakge
```
#!csharp
extractPackage.RunPackage();
```
![2723168481-TddFramework.jpeg](https://bitbucket.org/repo/o47ppy/images/2905333766-2723168481-TddFramework.jpeg)