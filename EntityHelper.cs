﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SSISTDD
{
    public class EntityHelper<T> where T : class
    {
        public T AddNewEntity(Dictionary<int, object> Entities)
        {
            int index = Entities.Count();
            Entities.Add(index, Activator.CreateInstance<T>());

            return (T)Entities[index];
        }

        public T SetDefaults(dynamic entity, params Dictionary<string, object>[] DefaultValues)
        {
            if (DefaultValues == null)
                return (T)entity;

            PropertyInfo[] columnsPropertyInfo = entity.GetType().GetProperties();
            Dictionary<string, PropertyInfo> columns = columnsPropertyInfo.ToDictionary(c => c.Name);
            bool ErrorsOccured = false;
            foreach (var defaultValueCollection in DefaultValues)
            {
                foreach (var kvp in defaultValueCollection)
                {
                    if (columns.ContainsKey(kvp.Key))
                        try
                        {
                            columns[kvp.Key].SetValue(entity, kvp.Value);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(string.Format("ERROR: Default value for {0} cannot be set on entity {1}: {2}", kvp.Key, typeof(T).FullName, e.Message));
                            ErrorsOccured = true;
                        }
                    else
                    {
                        Console.WriteLine(string.Format("ERROR: Default value for {0} cannot be set as it is not defined on entity {1}.", kvp.Key, typeof(T).FullName));
                        ErrorsOccured = true;
                    }
                }
            }
            if (!ErrorsOccured)
                return (T)entity;
            else
                throw new ArgumentException("Setting default values failed. See previous messages");
        }

        public string GetTableName(DbContext dbContext)
        {
            var context = (dbContext as IObjectContextAdapter).ObjectContext;
            string sql = context.CreateObjectSet<T>().ToTraceString();
            Regex regex = new Regex("FROM (?<table>.*) AS");
            Match match = regex.Match(sql);

            string table = match.Groups["table"].Value;
            if (table.IndexOf("] AS [") > 0)
                table = table.Substring(0, table.IndexOf("] AS [") + 1);
            return table;
        }
    }
}
